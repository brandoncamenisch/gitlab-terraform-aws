terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.44.0"
    }
  }
  backend "http" {
  }  
}

provider "aws" {
  region     = var.aws_region
  access_key = var.aws_access_id
  secret_key = var.aws_secret_key
}

module "ghost-lightsail" {
  source                 = "./ghost-lightsail"
  name                   = "ghost-lightsail-dev"
  aws_availability_zone  = var.aws_availability_zone
  domain_name            = var.domain_name
  key_pair_name          = var.key_pair_name
}