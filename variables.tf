variable "aws_access_id" {
}

variable "aws_secret_key" {
}

variable "aws_region" {
  default = "us-east-1"
}

variable "aws_availability_zone" {
  default = "us-east-1a"
}

variable "key_pair_name" {
  default = "default"
}

variable "domain_name" {
}