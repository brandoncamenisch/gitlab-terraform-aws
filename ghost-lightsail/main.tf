resource "aws_lightsail_instance" "ghost-dev" {
  name              = var.name
  availability_zone = var.aws_availability_zone
  blueprint_id      = "ghost_bitnami"
  bundle_id         = "nano_2_0"
  key_pair_name     = aws_lightsail_key_pair.ghost_key_pair.name
  tags = {
    env = "dev"
  }
}

 resource "aws_lightsail_key_pair" "ghost_key_pair" {
  name = var.key_pair_name
}

resource "aws_lightsail_static_ip" "ghost_static_ip" {
  name = "ghost_static_ip"
}

resource "aws_lightsail_static_ip_attachment" "static_ip_attach" {
  static_ip_name = aws_lightsail_static_ip.ghost_static_ip.id
  instance_name  = aws_lightsail_instance.ghost-dev.id
}

resource "aws_lightsail_domain" "domain_test" {
  domain_name = var.domain_name
}